#!/usr/bin/env sh
set -e

REPOSITORY_BASE_DIR="/var/package-repository"
INCOMING_DIR="${REPOSITORY_BASE_DIR}/incoming"
DEB_INCOMING="${INCOMING_DIR}/deb"
RPM_INCOMING="${INCOMING_DIR}/rpm"

for DEB_REPOSITORY in ${DEB_REPOSITORIES}; do
  IFS=":" read REPOSITORY DISTS <<EOF
${DEB_REPOSITORY}
EOF

  for DIST in $(echo "${DISTS}" | tr "," " " ); do
    REPOSITORY_PATH="${DEB_INCOMING}/${REPOSITORY}/${DIST}"

    if [ ! -d "${REPOSITORY_PATH}" ]; then
      echo "Creating incoming directory: ${REPOSITORY_PATH}"
      mkdir -p "${REPOSITORY_PATH}"
    fi

    chown deploy:deploy "${REPOSITORY_PATH}"
    chmod 0750 "${REPOSITORY_PATH}"
  done
done

for RPM_REPOSITORY in ${RPM_REPOSITORIES}; do
  REPOSITORY_PATH="${RPM_INCOMING}/${RPM_REPOSITORY}"
  if [ ! -d "${REPOSITORY_PATH}" ]; then
    echo "Creating incoming directory: ${REPOSITORY_PATH}"
    mkdir -p "${REPOSITORY_PATH}"
  fi

  chown deploy:deploy "${REPOSITORY_PATH}"
  chmod 0750 "${REPOSITORY_PATH}"
done

echo "${SSH_AUTHORIZED_KEYS}" | tr "," "\n" > /home/deploy/.ssh/authorized_keys

if [ -z "${SSH_HOST_KEY}" ]; then
  echo "No host key provided, generating one..."
  /usr/bin/ssh-keygen -q -t ed25519 -N "" -f /etc/ssh/ssh_host_ed25519_key
  echo "Host public key is:" $(cat /etc/ssh/ssh_host_ed25519_key.pub)
else
  install -m 0600 /dev/null /etc/ssh/ssh_host_ed25519_key
  echo "${SSH_HOST_KEY}" > /etc/ssh/ssh_host_ed25519_key
fi

echo "Starting sshd daemon..."
/usr/sbin/sshd -D -e
