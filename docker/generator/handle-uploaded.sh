#!/usr/bin/env bash
set -e

echo "Handeling uploads..."

DEB_INCOMING="${INCOMING_PATH}/deb"
RPM_INCOMING="${INCOMING_PATH}/rpm"

for DEB_REPOSITORY in ${DEB_REPOSITORIES}; do
  IFS=":" read REPOSITORY DISTS <<< "${DEB_REPOSITORY}"

  for DIST in $(echo "${DISTS}" | tr "," " " ); do
    INCOMING="${DEB_INCOMING}/${REPOSITORY}/${DIST}"
    REPOSITORY_PATH="${DEB_BASE_PATH}/${REPOSITORY}"
    COMPONENT_PATH="${REPOSITORY_PATH}/dists/${DIST}/${DEB_SECTION}/binary-${DEB_ARCH}"

    mv -fv "${INCOMING}/"*.deb "${COMPONENT_PATH}/"
  done

  echo
done

for RPM_REPOSITORY in ${RPM_REPOSITORIES}; do
  INCOMING="${RPM_INCOMING}/${RPM_REPOSITORY}"
  REPOSITORY_PATH="${RPM_BASE_PATH}/${RPM_REPOSITORY}"

  mv -fv "${INCOMING}/"*.rpm "${REPOSITORY_PATH}/"

  echo
done

echo "Removing unsupported files..."
find "${INCOMING_PATH}/" -type f -exec rm -fv {} \;

echo
