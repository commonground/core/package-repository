#!/usr/bin/env bash
set -e

TRIGGER_FILE=".done"

# Handle uploads if trigger file is present
if [ -f "${INCOMING_PATH}/${TRIGGER_FILE}" ]; then
  /usr/local/bin/handle-uploaded
fi

echo "Watching..."

/usr/bin/inotifywait --monitor --quiet --format "%f" --event "close_write" "${INCOMING_PATH}" |
  while read FILE; do
    if [ "${FILE}" = "${TRIGGER_FILE}"  ]; then
      echo "Process upload triggerd..."

      # Remove trigger file
      rm -f "${TRIGGER_FILE}"

      # Handle uploaded files
      /usr/local/bin/handle-uploaded

      # Generate repositories
      /usr/local/bin/generate
    fi
  done
