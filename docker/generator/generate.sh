#!/usr/bin/env bash
set -e

CREATEREPO="/usr/bin/createrepo --simple-md-filenames --compress-type gz"
LOCK_FILE=/var/lock/generate-repositories

echo "Generating repositories..."
echo

# Create lock file
touch ${LOCK_FILE}
exec {FD}>${LOCK_FILE}
flock --nonblock ${FD} || (echo "Can't create lock" && exit 1)

# Generate DEB repositories
for DEB_REPOSITORY in ${DEB_REPOSITORIES}; do
  IFS=":" read REPOSITORY DISTS <<< "${DEB_REPOSITORY}"
  REPOSITORY_PATH="${DEB_BASE_PATH}/${REPOSITORY}"

  cd "${REPOSITORY_PATH}"

  echo "Generating DEB repository '${REPOSITORY}'..."
  /usr/bin/apt-ftparchive generate "${APT_FTPARCHIVE_CONF}-${REPOSITORY}"

  for DIST in $(echo "${DISTS}" | tr "," " " ); do
    DIST_DIRECTORY="dists/${DIST}"

    /usr/bin/apt-ftparchive \
      --config-file "${APT_FTPARCHIVE_CONF}" \
      --option APT::FTPArchive::Release::Suite="${DIST}" \
      release "${DIST_DIRECTORY}" > "${DIST_DIRECTORY}/Release"
  done

  echo
done

# Generate RPM repositories
for RPM_REPOSITORY in ${RPM_REPOSITORIES}; do
  REPOSITORY_PATH="${RPM_BASE_PATH}/${RPM_REPOSITORY}"

  echo "Generating RPM repository '${RPM_REPOSITORY}'..."

  if [ ! -d "${REPOSITORY_PATH}" ]; then
    echo "Creating directory: ${REPOSITORY_PATH}"
    mkdir -p "${REPOSITORY_PATH}"
  fi

  cd "${REPOSITORY_PATH}"

  # Update repository
  ${CREATEREPO} --update "${REPOSITORY_PATH}"

  # Create repository if update failed
  if [ "${?}" -ne "0" ]; then
    ${CREATEREPO} "${REPOSITORY_PATH}"
  fi

  echo
done

# Remove lock file
rm -f ${LOCK_FILE}
