#!/usr/bin/env bash
set -e

REPOSITORY_BASE_PATH="/var/package-repository"
export INCOMING_PATH="${REPOSITORY_BASE_PATH}/incoming"
export REPOSITORIES_PATH="${REPOSITORY_BASE_PATH}/repositories"
export PUBLIC_PATH="${REPOSITORIES_PATH}/public"

export DEB_REPOSITORIES="${DEB_REPOSITORIES}"
export RPM_REPOSITORIES="${RPM_REPOSITORIES}"
export APT_FTPARCHIVE_CONF="/usr/local/etc/apt-ftparchive.conf"

export DEB_BASE_PATH="${PUBLIC_PATH}/deb"
export RPM_BASE_PATH="${PUBLIC_PATH}/rpm"

export DEB_SECTION="main"
export DEB_ARCH="amd64"

if [ ! -d "${REPOSITORIES_PATH}" ]; then
  mkdir -p "${REPOSITORIES_PATH}"
fi

for DEB_REPOSITORY in ${DEB_REPOSITORIES}; do
  IFS=":" read REPOSITORY DISTS <<< "${DEB_REPOSITORY}"
  if [ ! -f "${APT_FTPARCHIVE_CONF}-${REPOSITORY}" ]; then
    cp "${APT_FTPARCHIVE_CONF}" "${APT_FTPARCHIVE_CONF}-${REPOSITORY}"
  fi

  for DIST in $(echo "${DISTS}" | tr "," " " ); do
    REPOSITORY_PATH="${DEB_BASE_PATH}/${REPOSITORY}"
    DIST_DIRECTORY="dists/${DIST}"
    COMPONENT_PATH="${REPOSITORY_PATH}/${DIST_DIRECTORY}/${DEB_SECTION}/binary-${DEB_ARCH}"

    if [ ! -d "${COMPONENT_PATH}" ]; then
      echo "Creating directory: ${COMPONENT_PATH}"
      mkdir -p "${COMPONENT_PATH}"
    fi

    cat >> "${APT_FTPARCHIVE_CONF}-${REPOSITORY}" << EOF
Tree "${DIST_DIRECTORY}" {
  Sections "${DEB_SECTION}";
  Architectures "${DEB_ARCH}";
};
EOF
  done
done

/usr/local/bin/generate

/usr/local/bin/watch
