{{/*
Expand the name of the chart.
*/}}
{{- define "package-repository.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "package-repository.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "package-repository.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "package-repository.labels" -}}
helm.sh/chart: {{ include "package-repository.chart" . }}
{{ include "package-repository.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "package-repository.selectorLabels" -}}
app.kubernetes.io/name: {{ include "package-repository.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "package-repository.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "package-repository.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{- define "package-repository.upload.image" -}}
{{- if .Values.image.registry }}
{{- printf "%s/%s:%s" .Values.image.registry .Values.upload.imageRepository .Values.image.tag -}}
{{- else }}
{{- printf "%s:%s" .Values.upload.imageRepository .Values.image.tag -}}
{{- end }}
{{- end -}}

{{- define "package-repository.generator.image" -}}
{{- if .Values.image.registry }}
{{- printf "%s/%s:%s" .Values.image.registry .Values.generator.imageRepository .Values.image.tag -}}
{{- else }}
{{- printf "%s:%s" .Values.generator.imageRepository .Values.image.tag -}}
{{- end }}
{{- end -}}

{{- define "package-repository.web.image" -}}
{{- if .Values.image.registry }}
{{- printf "%s/%s:%s" .Values.image.registry .Values.web.imageRepository .Values.image.tag -}}
{{- else }}
{{- printf "%s:%s" .Values.web.imageRepository .Values.image.tag -}}
{{- end }}
{{- end -}}
