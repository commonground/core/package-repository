# Package repository

Tooling for deploying the package repository


## Deployment


```sh
# Generate ssh host key
ssh-keygen -t ed25519 -N "" -C "" -f deploy/ssh_host_ed25519_key

# Create Kubernetes secret
kubectl create secret generic package-repository --from-file=deploy/ssh_host_ed25519_key
```

```sh
# Generate GitLab ssh key for deployment
ssh-keygen -t ed25519 -N "" -C "GitLab-deploy" -f deploy/id_ed25519
```
