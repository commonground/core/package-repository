stages:
 - build
 - deploy

Build:
  stage: build
  before_script:
    - export IMAGE_REGISTRY="${CI_REGISTRY}/"
    - export IMAGE_TAG="${CI_COMMIT_SHORT_SHA}"
    - echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}
  script:
    - docker-compose build
    - docker-compose push
    - echo -n "${IMAGE_TAG}" > ci_build_image_tag.txt
  artifacts:
    paths:
      - ci_build_image_tag.txt
  tags:
    - cg
    - shell

Deploy review:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  before_script:
    - export IMAGE_TAG="$(cat ci_build_image_tag.txt)"
  script:
    - kubectl version
    - kubectl create namespace "${KUBE_NAMESPACE}" || true
    - helm upgrade package-repository helm/package-repository
        --namespace "${KUBE_NAMESPACE}"
        --install
        --set-string image.tag=${IMAGE_TAG}
        --set-string ingress.hosts={"${KUBE_NAMESPACE}.${KUBE_INGRESS_BASE_DOMAIN}"}
        --set-string deploymentAnnotations."app\.gitlab\.com/env"="${CI_ENVIRONMENT_SLUG}"
        --set-string deploymentAnnotations."app\.gitlab\.com/app"="${CI_PROJECT_PATH_SLUG}"
        --set service.upload.loadBalancerSourceRanges="{`echo "${INGRESS_UPLOAD_CIDRS}" | tr ":" ","`}"
        --values deploy/values-review.yaml
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://$KUBE_NAMESPACE.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: Stop review
    auto_stop_in: 2 days
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'
  dependencies:
    - Build
  tags:
    - cg
    - docker

Stop review:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  script:
    - helm uninstall --namespace "${KUBE_NAMESPACE}" package-repository
    - kubectl delete namespace "${KUBE_NAMESPACE}"
  environment:
    name: $CI_COMMIT_REF_NAME
    action: stop
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^review\/.*$/'
      when: manual
      allow_failure: true
  tags:
    - cg
    - docker

Deploy testing:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    KUBE_NAMESPACE: package-repository-testing
  before_script:
    - export IMAGE_TAG="$(cat ci_build_image_tag.txt)"
  script:
    - kubectl version
    - helm upgrade package-repository helm/package-repository
        --namespace "${KUBE_NAMESPACE}"
        --install
        --set-string image.tag=${IMAGE_TAG}
        --set-string deploymentAnnotations."app\.gitlab\.com/env"="${CI_ENVIRONMENT_SLUG}"
        --set-string deploymentAnnotations."app\.gitlab\.com/app"="${CI_PROJECT_PATH_SLUG}"
        --set service.upload.loadBalancerSourceRanges="{`echo "${INGRESS_UPLOAD_CIDRS}" | tr ":" ","`}"
        --values deploy/values-testing.yaml
  environment:
    name: testing
    url: https://packages.demoground.nl
    on_stop: Stop testing
    auto_stop_in: 2 days
  rules:
    - when: manual
      allow_failure: true
  dependencies:
    - Build
  tags:
    - cg
    - docker

Stop testing:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  variables:
    KUBE_NAMESPACE: package-repository-testing
  script:
    - helm uninstall --namespace "${KUBE_NAMESPACE}" package-repository
  environment:
    name: testing
    action: stop
  rules:
    - when: manual
      allow_failure: true
  tags:
    - cg
    - docker

Deploy production:
  stage: deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  before_script:
    - export IMAGE_TAG="$(cat ci_build_image_tag.txt)"
  script:
    - kubectl version
    - helm upgrade package-repository helm/package-repository
        --namespace "${KUBE_NAMESPACE}"
        --install
        --set-string image.tag=${IMAGE_TAG}
        --set-string deploymentAnnotations."app\.gitlab\.com/env"="${CI_ENVIRONMENT_SLUG}"
        --set-string deploymentAnnotations."app\.gitlab\.com/app"="${CI_PROJECT_PATH_SLUG}"
        --set service.upload.loadBalancerSourceRanges="{`echo "${INGRESS_UPLOAD_CIDRS}" | tr ":" ","`}"
        --values deploy/values-production.yaml
  environment:
    name: production
    url: https://packages.commonground.nl
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  dependencies:
    - Build
  tags:
    - cg
    - docker
